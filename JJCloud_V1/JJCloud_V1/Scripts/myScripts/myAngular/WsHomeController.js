﻿var app = angular.module('myApp', ['ngGrid', 'nvd3ChartDirectives']);

app.controller('WsTableController', ['$scope', function ($scope) {

    $(function () {
        // Reference the auto-generated proxy for the hub.
        $.connection.hub.url = "/signalr"

        var WsHub = $.connection.WsHub;
        WsHub.client.receiveConsultas = function (message) {
            //console.log(JSON.stringify(message));
            $scope.WsMessages.push(message);
            $scope.$apply();
        };

        // Start the connection.
        $.connection.hub.start({ transport: 'longPolling' }).done(function () {
            //console.log("Connection OK!");

            WsHub.server.getAllConsultas().done(function (consultas) {
                $scope.WsMessages = consultas;
                $scope.$apply();
            });

        });

    });

    $scope.gridOptions3 = {
        data: 'WsMessages',
        showFooter: false,
        showFilter: true,
        showHeader: true,
        showColumnMenu: false,
        showGroupPanel: false,
        showSelectionCheckbox: false,
        enablePaging: true,
        enableHighlighting: true,
        enableRowReordering: false,
        enableColumnResize: true,
        multiSelect: false,
        columnDefs: [{ field: 'WsName', displayName: 'Name' },
                     { field: 'WsType', displayName: 'Type' },
                     { field: 'WsResult', displayName: 'Result' },
                     { field: 'Date'}
        ]
    };

}]);

app.controller('graphicController', ['$scope', function ($scope) {


    $(function () {
        // Reference the auto-generated proxy for the hub.  
        $.connection.hub.url = "/signalr"

        var WsHub = $.connection.WsHub;

        WsHub.client.receiveWsTypePie = function (message) {
            $scope.myData = message;
            $scope.$apply();
        };

        WsHub.client.receiveWsNamePie = function (message) {
            //console.log(JSON.stringify(message));
            $scope.myData2 = message;
            $scope.$apply();
        };

        // Start the connection.
        $.connection.hub.start({ transport: 'longPolling' }).done(function () {
            //console.log("Connection OK!");

            WsHub.server.getWsNamePie().done(function (consultas) {
                $scope.myData2 = consultas;
            });

            WsHub.server.getWsTypePie().done(function (consultas) {
                $scope.myData = consultas; 
            });

            $scope.$apply();
        });

    });

    $scope.xFunction = function () {
        return function (d) {
            return d.Name;
        };
    }

    $scope.yFunction = function () {
        return function (d) {
            return d.Count;
        };
    }

}]);