﻿CREATE VIEW [dbo].[GadgetSlaveView]
AS
SELECT        A.Id AS SlaveID, A.Name AS SlaveName, B.Id AS MasterID, C.typeName AS SlaveType
FROM            dbo.GadgetSet AS A INNER JOIN
                         dbo.GadgetTypeSet AS C ON A.GadgetTypeId = C.Id INNER JOIN
                         dbo.GadgetSet AS B ON A.GadgetId = B.Id

GO

CREATE VIEW [dbo].[GagdetView]
AS
SELECT        dbo.GadgetSet.Id AS myID, dbo.GadgetSet.Name, dbo.GadgetTypeSet.typeName
FROM            dbo.GadgetSet INNER JOIN
                         dbo.GadgetTypeSet ON dbo.GadgetSet.GadgetTypeId = dbo.GadgetTypeSet.Id

GO