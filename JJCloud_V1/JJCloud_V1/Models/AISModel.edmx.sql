
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/22/2014 16:37:00
-- Generated from EDMX file: C:\Users\Joaquim\Documents\FCT\10 Semestre\AIS\Praticas\Trabalho Final\JJCloud_V1\JJCloud_V1\Models\AISModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [AIS.Ws];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_GadgetTypeGadget]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GadgetSet] DROP CONSTRAINT [FK_GadgetTypeGadget];
GO
IF OBJECT_ID(N'[dbo].[FK_GadgetLeituras]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LeiturasSet] DROP CONSTRAINT [FK_GadgetLeituras];
GO
IF OBJECT_ID(N'[dbo].[FK_GadgetHomeTemp]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[HomeTempSet] DROP CONSTRAINT [FK_GadgetHomeTemp];
GO
IF OBJECT_ID(N'[dbo].[FK_GadgetGadget]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GadgetSet] DROP CONSTRAINT [FK_GadgetGadget];
GO
IF OBJECT_ID(N'[dbo].[FK_GadgetLightStatus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LightStatusSet] DROP CONSTRAINT [FK_GadgetLightStatus];
GO
IF OBJECT_ID(N'[dbo].[FK_GadgetShopList]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ShopListSet] DROP CONSTRAINT [FK_GadgetShopList];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[GadgetSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GadgetSet];
GO
IF OBJECT_ID(N'[dbo].[LeiturasSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LeiturasSet];
GO
IF OBJECT_ID(N'[dbo].[ConsultasSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ConsultasSet];
GO
IF OBJECT_ID(N'[dbo].[GadgetTypeSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GadgetTypeSet];
GO
IF OBJECT_ID(N'[dbo].[HomeTempSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[HomeTempSet];
GO
IF OBJECT_ID(N'[dbo].[LightStatusSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LightStatusSet];
GO
IF OBJECT_ID(N'[dbo].[ShopListSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ShopListSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'GadgetSet'
CREATE TABLE [dbo].[GadgetSet] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [GadgetTypeId] int  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [GadgetId] bigint  NULL
);
GO

-- Creating table 'LeiturasSet'
CREATE TABLE [dbo].[LeiturasSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [GadgetId] bigint  NOT NULL,
    [value] nvarchar(max)  NOT NULL,
    [Date] datetime  NOT NULL
);
GO

-- Creating table 'ConsultasSet'
CREATE TABLE [dbo].[ConsultasSet] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [WsName] nvarchar(max)  NOT NULL,
    [WsResult] nvarchar(max)  NOT NULL,
    [WsType] nvarchar(max)  NOT NULL,
    [Date] datetime  NOT NULL
);
GO

-- Creating table 'GadgetTypeSet'
CREATE TABLE [dbo].[GadgetTypeSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [typeName] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'HomeTempSet'
CREATE TABLE [dbo].[HomeTempSet] (
    [Id] bigint  NOT NULL,
    [maxTemp] decimal(5,2)  NULL,
    [minTemp] decimal(5,2)  NULL,
    [actualTemp] decimal(5,2)  NULL,
    [FireAlarm] bit  NOT NULL,
    [GarageClear] bit  NOT NULL
);
GO

-- Creating table 'LightStatusSet'
CREATE TABLE [dbo].[LightStatusSet] (
    [Id] bigint  NOT NULL,
    [Status] bit  NOT NULL
);
GO

-- Creating table 'ShopListSet'
CREATE TABLE [dbo].[ShopListSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [List] nvarchar(max)  NOT NULL,
    [GadgetId] bigint  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'GadgetSet'
ALTER TABLE [dbo].[GadgetSet]
ADD CONSTRAINT [PK_GadgetSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LeiturasSet'
ALTER TABLE [dbo].[LeiturasSet]
ADD CONSTRAINT [PK_LeiturasSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ConsultasSet'
ALTER TABLE [dbo].[ConsultasSet]
ADD CONSTRAINT [PK_ConsultasSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'GadgetTypeSet'
ALTER TABLE [dbo].[GadgetTypeSet]
ADD CONSTRAINT [PK_GadgetTypeSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'HomeTempSet'
ALTER TABLE [dbo].[HomeTempSet]
ADD CONSTRAINT [PK_HomeTempSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LightStatusSet'
ALTER TABLE [dbo].[LightStatusSet]
ADD CONSTRAINT [PK_LightStatusSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ShopListSet'
ALTER TABLE [dbo].[ShopListSet]
ADD CONSTRAINT [PK_ShopListSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [GadgetTypeId] in table 'GadgetSet'
ALTER TABLE [dbo].[GadgetSet]
ADD CONSTRAINT [FK_GadgetTypeGadget]
    FOREIGN KEY ([GadgetTypeId])
    REFERENCES [dbo].[GadgetTypeSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GadgetTypeGadget'
CREATE INDEX [IX_FK_GadgetTypeGadget]
ON [dbo].[GadgetSet]
    ([GadgetTypeId]);
GO

-- Creating foreign key on [GadgetId] in table 'LeiturasSet'
ALTER TABLE [dbo].[LeiturasSet]
ADD CONSTRAINT [FK_GadgetLeituras]
    FOREIGN KEY ([GadgetId])
    REFERENCES [dbo].[GadgetSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GadgetLeituras'
CREATE INDEX [IX_FK_GadgetLeituras]
ON [dbo].[LeiturasSet]
    ([GadgetId]);
GO

-- Creating foreign key on [Id] in table 'HomeTempSet'
ALTER TABLE [dbo].[HomeTempSet]
ADD CONSTRAINT [FK_GadgetHomeTemp]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[GadgetSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [GadgetId] in table 'GadgetSet'
ALTER TABLE [dbo].[GadgetSet]
ADD CONSTRAINT [FK_GadgetGadget]
    FOREIGN KEY ([GadgetId])
    REFERENCES [dbo].[GadgetSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GadgetGadget'
CREATE INDEX [IX_FK_GadgetGadget]
ON [dbo].[GadgetSet]
    ([GadgetId]);
GO

-- Creating foreign key on [Id] in table 'LightStatusSet'
ALTER TABLE [dbo].[LightStatusSet]
ADD CONSTRAINT [FK_GadgetLightStatus]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[GadgetSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [GadgetId] in table 'ShopListSet'
ALTER TABLE [dbo].[ShopListSet]
ADD CONSTRAINT [FK_GadgetShopList]
    FOREIGN KEY ([GadgetId])
    REFERENCES [dbo].[GadgetSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GadgetShopList'
CREATE INDEX [IX_FK_GadgetShopList]
ON [dbo].[ShopListSet]
    ([GadgetId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------