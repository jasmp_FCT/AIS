﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JJCloud_V1.Models.Interfaces
{
    interface IHomeTempRepository
    {
        HomeTemp Get(long id);
        HomeTemp Add(HomeTemp item);
        bool Update(HomeTemp item);
    }
}
