﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JJCloud_V1.Models.Interfaces
{
    interface IShopListRepository
    {
        IEnumerable<ShopList> GetAll(long id);
        ShopList Add(ShopList item);
    }
}
