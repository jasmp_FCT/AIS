﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using JJCloud_V1.Models.Interfaces;
using System;
using JJCloud_V1.Hubs;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace JJCloud_V1.Models.Repositorys
{
    public class ConsultasRepository : BigRepository, IConsultasRepository
    {
        private IHubContext _hubContext = GlobalHost.ConnectionManager.GetHubContext<WSHub>();
        public IEnumerable<Consultas> GetAll()
        {
            return db.ConsultasSet;
        }
        public IEnumerable<Consultas> GetLast(int Last)
        {
            return db.ConsultasSet.Take(Last);
        }
        public IEnumerable<GraphicPies> Count(bool WsType)
        {
            IEnumerable<GraphicPies> res = new List<GraphicPies>();
            List<Consultas> consultas = db.ConsultasSet.ToList();

            if (WsType) {
                var categoryCounts = from p in consultas 
                                 group p by p.WsType into g 
                                 select new GraphicPies{ Name = g.Key, Count = g.Count() };
                res = categoryCounts.AsEnumerable();
            }
            else
            {
                var categoryCounts = from p in consultas
                                     group p by p.WsName into g
                                     select new GraphicPies { Name = g.Key, Count = g.Count() };
                res = categoryCounts.AsEnumerable();
            }


            return res;
        }
        public Consultas Get(long id)
        {
            return db.ConsultasSet.Find(id);
        }
        public bool Add(Consultas item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            Consultas aux = db.ConsultasSet.Add(item);
            bool resfinal = db.SaveChanges() == 1 ? true : false;
            if (resfinal)
            {
                _hubContext.Clients.All.receiveConsultas(aux);
                _hubContext.Clients.All.receiveWsNamePie(Count(false));
                _hubContext.Clients.All.receiveWsTypePie(Count(true));
            }
            return resfinal;
        }
        public bool Remove(long id)
        {
            db.ConsultasSet.Remove(db.ConsultasSet.Find(id));
            return db.SaveChanges() == 1 ? true : false;
        }
        public bool Update(Consultas item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            Consultas res = db.ConsultasSet.Find(item.Id);
            res.WsName = item.WsName;
            res.WsResult = item.WsResult;
            res.WsType = item.WsType;
            bool resfinal = db.SaveChanges() == 1 ? true : false;
            if (resfinal)
            {
                _hubContext.Clients.All.receiveConsultas(GetAll());
            }
            return resfinal;
        }

    }
}