﻿using JJCloud_V1.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JJCloud_V1.Models.Repositorys
{
    public class HomeTempRepository : BigRepository, IHomeTempRepository
    {
        public HomeTemp Get(long id)
        {
            return db.HomeTempSet.Find(id);
        }

        public HomeTemp Add(HomeTemp item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            HomeTemp res = db.HomeTempSet.Add(item);
            db.SaveChanges();

            return res;
        }

        public bool Update(HomeTemp item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            HomeTemp res = db.HomeTempSet.Find(item.Id);
            res.maxTemp = item.maxTemp;
            res.minTemp = item.minTemp;
            res.FireAlarm = item.FireAlarm;
            res.GarageClear = item.GarageClear;

            return db.SaveChanges() == 1 ? true : false;
        }
    }
}