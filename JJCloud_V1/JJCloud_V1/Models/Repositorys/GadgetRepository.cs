﻿using JJCloud_V1.Hubs;
using JJCloud_V1.Models.Interfaces;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JJCloud_V1.Models.Repositorys
{
    public class GadgetRepository : BigRepository, IGadgetRepository
    {
        private IHubContext _hubContext = GlobalHost.ConnectionManager.GetHubContext<WSHub>();
        public IEnumerable<Gadget> GetAll()
        {
            return null;
        }
        public IEnumerable<GagdetView> GetAllView()
        {
            return db.GagdetView;
        }
        public IEnumerable<GraphicPies> Count(bool Master, long masterID =0)
        {
            IEnumerable<GraphicPies> res = new List<GraphicPies>();


            if (Master)
            {
                List<GagdetView> consultas = db.GagdetView.ToList();
                var categoryCounts = from p in consultas
                                     group p by p.typeName into g
                                     select new GraphicPies { Name = g.Key, Count = g.Count() };
                res = categoryCounts.AsEnumerable();
            }
            else
            {
                List<GadgetSlaveView> consultas = db.GadgetSlaveView.ToList();
                var categoryCounts = from p in consultas
                                     where p.MasterID == masterID
                                     group p by p.SlaveType into g
                                     select new GraphicPies { Name = g.Key, Count = g.Count() };
                res = categoryCounts.AsEnumerable();
            }


            return res;
        }
        public IEnumerable<GadgetSlaveView> GetAllSlaves(long MasterID)
        {
            return db.GadgetSlaveView.Where(b => b.MasterID == MasterID);
        }
        public IEnumerable<Gadget> GetAllFrom(long houseID){
            return db.GadgetSet.Where(b => b.GadgetId == houseID );
        }
        public IEnumerable<Gadget> GetAllFrom(long houseID, long typeID)
        {
            return db.GadgetSet.Where(b => (b.GadgetId== houseID && b.GadgetTypeId == typeID));
        }
        public Gadget add(string name, GadgetType gT){

            if (String.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }
            Gadget res = db.GadgetSet.Add(new Gadget { Name = name, GadgetTypeId = gT.Id });
            if (db.SaveChanges() == 1)
            {
                _hubContext.Clients.All.receiveGadgets(db.GagdetView.Where(b => b.myID == res.Id).FirstOrDefault());
                _hubContext.Clients.All.receiveMasterPie(Count(true));
            }

            return res;
        }
        public Gadget Get(long id)
        {
            return db.GadgetSet.Find(id);
        }
        public bool Update(Gadget item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            Gadget res = db.GadgetSet.Find(item.Id);
            res.GadgetId = item.GadgetId;
            
            return db.SaveChanges() == 1 ? true : false;

        }
    }
}