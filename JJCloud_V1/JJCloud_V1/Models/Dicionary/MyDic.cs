﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JJCloud_V1.Models.Dicionary
{
    public class MyDic
    {
        public const string HOME = "HOME";
        public const string CARRO = "CARRO";
        public const string LUZ = "LUZ";
        public const string AC = "AC";

        public const string TEMPERATURE = "Temperature";
        public const string Temperature_Set = "Temperature_Set";
        public const string Temperature_Alter = "Temperature_Alter";
        public const string Temperature_Status = "Temperature_Status";
        public const string Temperature_Update = "Temperature_Update";

        public const string HOMEWS = "HOME";
        public const string Access_Request = "Access_Request";
        public const string Access_Request_Put = "Access_Request_Put";
        public const string Get_Device_List = "Get_Device_List";
        public const string Fire_Alarm_Put = "Fire_Alarm_Put";
        public const string Fire_Alarm = "Fire_Alarm";
        public const string Light_Update_Status = "Light_Update_Status";
        public const string Light_Status = "Light_Status";
        public const string Light_Update = "Light_Update";
        public const string Purchase_Order = "Purchase_Order";

        public const string Plataform = "Plataform";
        public const string Register_User = "Register_User";
        public const string Entity_Association = "Entity_Association";

    }
}