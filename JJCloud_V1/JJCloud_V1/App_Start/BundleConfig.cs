﻿using System.Web;
using System.Web.Optimization;

namespace JJCloud_V1
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/myScripts/angular.js",
                        "~/Scripts/jquery.signalR-2.1.0.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/myChanges").Include(
                "~/Content/myContent/bootstrap-theme.min.css",
                "~/Content/myContent/bootstrapMyChanges.css",
                "~/Content/myContent/ng-grid.css",
                "~/Content/myContent/nv.d3.css"
                ));

            bundles.Add(new ScriptBundle("~/bundles/ng-grid").Include(
                "~/Scripts/myScripts/ng-grid-2.0.11.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/d3").Include(
                "~/Scripts/myScripts/d3.js",
                "~/Scripts/myScripts/nv.d3.js",
                "~/Scripts/myScripts/angularjs-nvd3-directives.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/myAngularJSController").Include(
                 "~/signalr/hubs", 
                 "~/Scripts/myScripts/myAngular/HomeControllers.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/myAngularJSController2").Include(
                "~/signalr/hubs",    
                "~/Scripts/myScripts/myAngular/WsHomeController.js"
                    
            ));



        }
    }
}
